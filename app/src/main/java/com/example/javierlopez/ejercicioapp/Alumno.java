package com.example.javierlopez.ejercicioapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Javier Lopez on 26/07/2017.
 */

public class Alumno extends AppCompatActivity {

    private EditText edad;
    private RadioButton si, no, masculino, femenino;
    private CheckBox estudia, trabaja;

    private List<Alumno> listado;

    public Alumno() {
    }

    public EditText getEdad() {
        return edad;
    }

    public void setEdad(EditText edad) {
        this.edad = edad;
    }

    public RadioButton getSi() {
        return si;
    }

    public void setSi(RadioButton si) {
        this.si = si;
    }

    public RadioButton getNo() {
        return no;
    }

    public void setNo(RadioButton no) {
        this.no = no;
    }

    public RadioButton getMasculino() {
        return masculino;
    }

    public void setMasculino(RadioButton masculino) {
        this.masculino = masculino;
    }

    public RadioButton getFemenino() {
        return femenino;
    }

    public void setFemenino(RadioButton femenino) {
        this.femenino = femenino;
    }

    public CheckBox getEstudia() {
        return estudia;
    }

    public void setEstudia(CheckBox estudia) {
        this.estudia = estudia;
    }

    public CheckBox getTrabaja() {
        return trabaja;
    }

    public void setTrabaja(CheckBox trabaja) {
        this.trabaja = trabaja;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alumno);
        edad = (EditText) findViewById(R.id.edad);
        si = (RadioButton) findViewById(R.id.si);
        no = (RadioButton) findViewById(R.id.no);
        masculino = (RadioButton) findViewById(R.id.masculino);
        femenino = (RadioButton) findViewById(R.id.femenino);
        estudia = (CheckBox) findViewById(R.id.estudia);
        trabaja = (CheckBox) findViewById(R.id.trabaja);
    }

    public void guardarInfo(View view){
        int gEdad = Integer.parseInt(edad.getText().toString());
        boolean gSi = Boolean.parseBoolean(si.getText().toString());
        boolean gNo = Boolean.parseBoolean(no.getText().toString());
        boolean gMasculino = Boolean.parseBoolean(masculino.getText().toString());
        boolean gFemenino = Boolean.parseBoolean(femenino.getText().toString());
        boolean gEstudia = Boolean.parseBoolean(estudia.getText().toString());
        boolean gTrabaja = Boolean.parseBoolean(trabaja.getText().toString());

        Alumno al = new Alumno();
        al.setEdad(edad);
        al.setSi(si);
        al.setNo(no);
        al.setTrabaja(trabaja);
        al.setEstudia(estudia);
        al.setMasculino(masculino);
        al.setMasculino(masculino);
        al.setFemenino(femenino);

        listado.add(al);
        Toast notification = Toast.makeText(this, "Datos Almacenados Exitosamente ", Toast.LENGTH_SHORT);
        notification.show();
    }
}
